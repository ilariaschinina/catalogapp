#!/bin/bash

# Kill old container
docker kill catalogapp

docker rm catalogapp

docker pull ilariaschinina/catalogapp:latest

# We want to restart the container unless stopped,
# Export port 80 to server port 8080
# bind mount the database folder and two config files
# Give the container the name catalogapp
# and pull and run the latest catalogapp

docker run -d --restart=unless-stopped \
	-p 127.0.0.1:8080:80/tcp \
	--mount type=bind,source=/home/pooka/catalog-app/database,target=/app/database \
	--mount type=bind,source=/home/pooka/catalog-app/client_secret.json,target=/app/client_secret.json \
	--mount type=bind,source=/home/pooka/catalog-app/config.py,target=/app/config.py \
	--name catalogapp \
	ilariaschinina/catalogapp:latest
