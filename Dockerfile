FROM tiangolo/uwsgi-nginx-flask:python3.8

RUN mkdir /app/static
RUN mkdir /app/database

WORKDIR /app

# Install Node and npm
RUN apt-get update && apt-get install -y nodejs npm pipenv \
&& rm -rf /var/lib/apt/lists/*

# Install Sass
RUN npm install -g sass

# First copy npm definition first so we don't reinstall node_modules all the time
COPY static/package.json static/package-lock.json /app/static/
# Then install them
RUN cd static && npm install

# Then lets assume that our python dependencies change rarely and create its own
# docker image layer for this
COPY Pipfile Pipfile.lock /app/

# Install python dependencies
RUN pipenv install --system

RUN rm Pipfile Pipfile.lock

# Now copy files
COPY . /app/

